import pygame, sys
from pygame.locals import *
from transitions import Machine
from qhue import Bridge

pygame.init()
pygame.display.set_caption('HueLight')

ws = pygame.display.set_mode((500, 500))

b = Bridge("192.168.86.21","nUS6rndMcOE0XIWIVsxMA8EA-0xJH4Yqk1A-hCDf")
lights = b.lights[3]

ws.fill((0, 0, 0))

FPS = 30
fpsClock = pygame.time.Clock()
frameNum = 1;

redHue =0
yellowHue = 12750
greenHue = 25500
blueHue = 46900
purpleHue = 56100

class Hue:

    states = ['red', 'yellow', 'green', 'blue', 'purple']

    def __init__(self):

        lights.state(on = True )

        self.lastTick = pygame.time.get_ticks()
        self.pressedG = False

        self.machine = Machine(model=self, states=Hue.states, initial='green')
        self.machine.add_transition(trigger='goYellow',source='*',dest='yellow')
        self.machine.add_transition(trigger='goGreen',source='*',dest='green')
        self.machine.add_transition(trigger='goBlue',source='*',dest='blue')
        self.machine.add_transition(trigger='goPurple',source='*',dest='purple')
        self.machine.add_transition(trigger='goRed',source='*',dest='red')



    def setState(self):
        self.lastTick = pygame.time.get_ticks()

    def setGreen(self, setBoolean):
        self.pressedG = setBoolean



    def update(self):
        if self.state == 'green':
            lights.state(hue = greenHue)
            self.pressedG = False
            if pygame.time.get_ticks() >= self.lastTick + 5000:
                self.setState()
                self.goYellow()
        if self.state == 'yellow':
            lights.state(hue = yellowHue)
            if pygame.time.get_ticks() >= self.lastTick + 5000:
                self.setState()
                self.goBlue()
        if self.state == 'blue':
            lights.state(hue = blueHue)
            if pygame.time.get_ticks() >= self.lastTick + 5000:
                self.setState()
                self.goPurple()
        if self.state == 'purple':
            lights.state(hue = blueHue)
            if pygame.time.get_ticks() >= self.lastTick + 10000:
                if self.pressedG == True:
                    self.setState()
                    self.goGreen()

        if self.state == 'red':
            lights.state(hue = redHue)
            if pygame.time.get_ticks() >= self.lastTick + 5000:
                self.setState()
                self.goGreen()
        print (self.state)
        print (self.pressedG)





hueLight = Hue()


while True:

    ws.fill((0, 0, 0))

    hueLight.update()
    # print hueLight.setstate()


    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                hueLight.setState()
                hueLight.goRed()
            elif hueLight.state == "purple":
                if event.key == pygame.K_g:
                    hueLight.setGreen(True)


    pygame.display.update()
    fpsClock.tick(FPS)
