import pygame
from pygame.locals import *
from numpy import *
import pytweening as tween

pygame.init()
pygame.display.set_caption('Monty Python')
ds = pygame.display.set_mode((400, 500))

montyX = 10
montyY = 5

FPS = 30 # frames per second setting
fpsClock = pygame.time.Clock()

tMin = 0
tMax = 1

mMin = 180
mMax = 300

direction = 'down'


image = pygame.image.load('montypython.jpg')

pixels = pygame.surfarray.array3d(image)
print(pixels.shape)
pixels = pixels[260:-280, 680:-180]    # crop using array slice

pixelsurface = pygame.surfarray.make_surface(pixels)


while True: # the main game loop
    ds.fill((0, 0, 0))

    tSpan = tMax - tMin
    mSpan = mMax - mMin

    for i in range(10):
        value = (tween.easeInSine(i/(10)))
        valueScaled = float(value - tMin) / float(tSpan)
        mValue = mMin + (valueScaled * mSpan)

    if direction == 'down':
        montyY += 10
        if montyY >= 180:
            montyY = mValue
            if montyY >= 270:
                direction = 'up'
    elif direction == 'up':
        montyY -=10
        if montyY <= 5:
            direction = 'down'

    print (montyY)
    print (direction)

    ds.blit(pixelsurface, (montyX, montyY))


    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.update()
    fpsClock.tick(FPS)
