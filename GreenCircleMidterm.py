import pygame, sys
from pygame.locals import *

pygame.init()
pygame.display.set_caption('GreenCircle')

W = 500
H = 500
ws = pygame.display.set_mode((W, H))

ws.fill((0, 0, 0))
greenColor = (0,255,0)


pygame.draw.circle(ws, greenColor, (250, 250), 50)

while True:

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.update()
